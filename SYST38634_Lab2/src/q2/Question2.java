package q2;

public class Question2 {

	public static void main(String[] args) {
		
		//int[] arr = {25, 37, 29}; 
		//int[] arr = {25, 45, 65};
		 
		//System.out.println("The smallest value is " + getSmallest(arr));
		//System.out.println("The average value is " + getAverage(arr));
		//System.out.println("The middle character in the string: " + getMiddle("350"));		
		//System.out.println("Number of vowels in the string: " + getVowelCount("w3resource"));
		//System.out.println("The character at position 10 is " + getIndexChar("Java Exercises!", 10));
		
		System.out.println("Codepoint count = " + getUnicodePoints("w3rsource.com"));
	}
	
	public static int getUnicodePoints(String str) {
				
		return str.codePointCount(0, str.length());
		
	}
	
	public static char getIndexChar(String str, int index) {
		
		return str.charAt(index);
		
	}
	
	public static int getVowelCount(String str) {
		
		int vowelNum = 0;
		
		for (int i = 0; i < str.length(); i++) {
			
			char c = str.charAt(i);
			
			if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
				vowelNum++;
			}
		}
		
		return vowelNum;
	}

	public static int getSmallest(int[] arr) {

		int smallestNum = arr[0];

		for (int i = 0; i < arr.length; i++) {

			if (arr[i] < smallestNum) {
				smallestNum = arr[i];
			}

		}

		return smallestNum;

	}

	public static double getAverage(int[] arr) {

		double total = 0.0;

		for (int i = 0; i < arr.length; i++) {
			total += arr[i];
		}

		return total / arr.length;
	}

	public static String getMiddle(String str) {

		int midNum = str.length() / 2;
		String midChar = "";

		if (str.length() % 2 == 0) {
			midChar = Character.toString(str.charAt(midNum - 1)) + Character.toString(str.charAt(midNum));
		} else {
			midChar = Character.toString(str.charAt(midNum));
		}

		return midChar;
	}

}
